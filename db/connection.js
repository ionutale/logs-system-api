const mongoose = require('mongoose')
mongoose.Promise = global.Promise

const dbURI = process.env.DOCKER_MONGODB

const options = {
    autoIndex: false,
    reconnectTries: Number.MAX_VALUE, // never stop trying
    reconnectInterval: 500, 
    poolSize: 10,
    bufferMaxEntries: 0,
    useNewUrlParser: true 
}

mongoose.connect(dbURI, options)

// CONNECTION EVENTS
mongoose.connection.on('connected', (err, info) => console.log(`mongoose connnected to: ${dbURI}`))
mongoose.connection.on('error', (err, info) => {
    console.log("\x1b[31m",`mongoose connnection ERROR: ${dbURI}\n`, err, info)
    process.exit(0)
})
mongoose.connection.on('disconnected', () => console.log(`mongoose disconnected`))

// For nodemon restarts
process.once('SIGUSR2', () => {
    gracefulShutdown('nodemon restart', () => {
        process.kill(process.pid, 'SIGURS2')
    })
})

// For app termination
process.on('SIGINT', () => {
    gracefulShutdown('app termination', () => {
        process.exit(0)
    })
})

// For Heroku app termination 
process.on('SIGTERM', () => {
    gracefulShutdown('Heroku app termination', () => {
        process.exit(0)
    })
})

// add schema models here
require('./models/logs.model')
