const mongoose = require('mongoose')
const LOG = 'logs'

module.exports.createLog = async (req, res, next) => {
	const Log = mongoose.model(LOG)
	console.log(req.body)
	try {
		const {text = 'no text from body', service = 'no text from body', identifier = 'no text from body', tags = []} = req.body
		const log = new Log()
		
		log.text = text
		log.service = service
		log.identifier = identifier
		log.tags = tags

		const dbSaveRes = await log.save()	
		res.status(201).json(dbSaveRes);

	} catch (error) {
		console.log(error)
		res.status(422).json(customError(`Unable to create new ${LOG}. `, 422, error.message));
	}
}

