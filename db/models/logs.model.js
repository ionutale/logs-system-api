const mongoose = require( 'mongoose' );

const logsSchema = new mongoose.Schema({
	text: String,
	service: String, // this should be an ip or a name
	identifier: String, // this should be the method name, event name, 
  tags: {type: Array, index: true},
  timestamp: {type: Date, index: true, default: Date.now, expires: '30d'}
});

module.exports = mongoose.model("logs", logsSchema);
