const express = require('express')
const app = express()
const logsRouter = require('./routers/logs')
const logger = require('morgan')
const bodyParser = require('body-parser')
const apiMetrics = require('prometheus-api-metrics');
const debug = require('debug')

require('./db/connection')
app.use(logger('combined'))
app.use(apiMetrics())

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));

app.get('/', (req, res, next)=> {
	console.log('headers', req.headers)
	console.log(req.app.get('env'))

	res.status(200).json({'msg': 'ok'})
})

app.use('/logs', logsRouter)

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
/* eslint no-unused-vars: 0 */
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500).json(err)
});
// Handle uncaughtException
process.on('uncaughtException', (err) => {
  debug('Caught exception: %j', err);
  process.exit(1);
});

app.listen(process.env.PORT || '3102')