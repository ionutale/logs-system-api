const express = require('express')
const router = express.Router()
const logsColl = require('../db/queries/log.query')
const logValidator = require('./validators/logs.validator')

router.post('/', logValidator.logCreate, logsColl.createLog)

router.all('/', (req, res, next) => {
	res.status(501).json({
		'code': '501',
		'Description': 'this method for logs has not been implemented. The only method right now is POST'
	})
})


module.exports = router