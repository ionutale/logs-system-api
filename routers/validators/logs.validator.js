const Joi = require('joi')
const { customError } = require('../../errorHandler/errorHandler')

const logSchema = Joi.object().keys({
	text: Joi.string().default(''),
	service: Joi.string().default(''),
	identifier: Joi.string().default(''),
	tags: Joi.array().default([])
})

module.exports.none = (req, res, next) => {
	try {
		next()
	} catch (error) {
		console.log(error)
	}
}

module.exports.logCreate = (req, res, next) => {
	try {
		const log = req.body
		const validation = Joi.validate(log, logSchema)

		if (validation.error === null) {
			next()
		} else {
			res.status(422).json(customError(validation.error.name, 422, validation.error.details[0].message))
		}
	} catch (error) {
		console.log(error)
	}
}	
