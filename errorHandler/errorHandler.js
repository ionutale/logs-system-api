module.exports.bad_request = (req, res, next) => {
    const err = new Error();
    err.status = 400;
    err.message = 'Bad Request';
    res.status(err.status).json(err);
}

module.exports.unauthorized = (req, res, next) => {
    const err = new Error();
    err.status = 401;
    err.message = 'Unauthorized';
    res.status(err.status).json(err);
}

module.exports.forbidden = (req, res, next) => {
    const err = new Error();
    err.status = 403;
    err.message = 'Forbidden';
    res.status(err.status).json(err);
}

module.exports.not_found = (req, res, next) => {
    const err = new Error();
    err.status = 404;
    err.message = 'Not Found';
    res.status(err.status).json(err);
}

module.exports.method_not_allowed = (req, res, next) => {
    const err = new Error();
    err.status = 405;
    err.message = 'Method Not Allowed';
    res.status(err.status).json(err);
}

module.exports.customError = (message = "Bad Request", status = 400, description = "") => {
    const err = new Error();
    err.message = message
    err.code = status;
    err.description = description;
    return err
}

// generic error handler
module.exports.genericErrorHandler = ({ genericErrorMessage = "this looks like a compilation error on server side" } = {}) => {
    return (error, req, res, next) => {
        if (process.env.NODE_ENV !== "test") {
            console.error({
                error,
                message: error.message || genericErrorMessage
            });
        }

        if (error.status) {
            return res.status(error.status).json({
                type: error.name,
                message: error.message,
                errors: error.errors
            });
        } else {
            console.log("NODE_ENV:", process.env.NODE_ENV)
            return process.env.NODE_ENV !== 'prod' ?
                res.status(500).json({ err_msg: error.message, message: genericErrorMessage }) : res.status(500).json({ message: genericErrorMessage });
        }
    };
};
